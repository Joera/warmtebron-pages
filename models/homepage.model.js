
const logger = require('../../services/logger.service');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'homepage',
            template: 'homepage.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {
        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.url, // replace wordpress generated url for page
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            date: data.date,
            modified: data.modified,
            render_environments: data.render_environments || ['']
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.url,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            render_environments: data.render_environments,
            language: data.language
        }

        return searchObject;
    }
}
