const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'projectteam',
            template: 'projectteam.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.link, // replace wordpress generated url for page
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            date: data.date,
            modified: data.modified,
            language: data.language,
            render_environments: data.render_environments || ['']

        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            language: data.language ||  { 'code' : 'nl'}

        };

        // trim sections to text only
        return searchObject;
    }
}
