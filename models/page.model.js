const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'page',
            template: 'page.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        if (data._id === '279') {
            data.interaction.nested_comments = data.interaction.nested_comments.reverse();
        }

        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.link, // replace wordpress generated url for page
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            date: data.date,
            modified: data.modified,
            author: data.author,
            sections: data.sections,
            main_image: data.main_image,
            interaction: data.interaction,
            children: data.children,
            subtitle: data.subtitle,
            language: data.language,
            render_environments: data.render_environments || ['']

        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            sections: data.sections,
            main_image: data.main_image,
            render_environments: data.render_environments,
            language: data.language ||  { 'code' : 'nl'}

        };

        // trim sections to text only
        Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject;
    }
}
