const logger = require('../../services/logger.service');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'partner',
            template: 'partner.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.link, // replace wordpress generated url for page
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                date: data.date,
                modified: data.modified,
                main_image: data.main_image,
                taxonomies: data.taxonomies,
                catIds: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.id }) : [],
                catSlugs: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.slug }) : [],
                partner_url: data.partner_url,
                render_environments: data.render_environments || ['']
            };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            sections: data.sections,
            render_environments: data.render_environments,
            language: data.language ||  { 'code' : 'nl'}

        };

        return searchObject;
    }
}
