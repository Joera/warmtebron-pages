'use strict';

/**
 * Remove json data from the news queue
 */
module.exports = function() {

    let gulp = require('gulp'),
        babel = require('gulp-babel'),
        plumber = require('gulp-plumber'),
        concat = require('gulp-concat'),
        merge = require('merge-stream'),
        path = require('../local-path')();

    let scriptsDir = path.distFolder + '/assets/scripts/';

    gulp.task('js-compile', function() {

        var post = gulp.src([
                path.projectFolder + '/scripts/_loadJSON.js',
                path.projectFolder + '/scripts/polyfill.js',
                path.projectFolder + '/scripts/detect.js',
                path.projectFolder + '/scripts/menu.js',
                path.projectFolder + '/scripts/commenting.js',
                path.projectFolder + '/scripts/star-rating.js',
                path.projectFolder + '/scripts/pageviews.js',
                path.projectFolder + '/scripts/video.js',
                path.projectFolder + '/scripts/newsletter.js',
                path.projectFolder + '/scripts/social-dialogue.js'

            ])
            .pipe(plumber())
            .pipe(concat('post.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var homepage = gulp.src([
                path.projectFolder + '/scripts/_loadJSON.js',
                path.projectFolder + '/scripts/polyfill.js',
                path.projectFolder + '/scripts/detect.js',
                path.projectFolder + '/scripts/waypoints.js',
                path.projectFolder + '/scripts/homepage.js',
                path.projectFolder + '/scripts/menu.js',
                path.projectFolder + '/scripts/newsletter.js',
                path.projectFolder + '/scripts/newsletter-settings.js'
            ])
            // .pipe(plumber())
            .pipe(concat('homepage.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var page = gulp.src([
                path.projectFolder + '/scripts/_loadJSON.js',
                path.projectFolder + '/scripts/polyfill.js',
                path.projectFolder + '/scripts/detect.js',
                path.projectFolder + '/scripts/menu.js',
                path.projectFolder + '/scripts/commenting.js',
                path.projectFolder + '/scripts/star-rating.js',
                path.projectFolder + '/scripts/pageviews.js',
                path.projectFolder + '/scripts/video.js',
                path.projectFolder + '/scripts/newsletter.js',
                path.projectFolder + '/scripts/social-dialogue.js'
            ])
            // .pipe(plumber())
            .pipe(concat('page.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var projectteam = gulp.src([
            path.projectFolder + '/scripts/_loadJSON.js',
            path.projectFolder + '/scripts/polyfill.js',
            path.projectFolder + '/scripts/detect.js',
            path.projectFolder + '/scripts/menu.js',
            // path.projectFolder + '/scripts/commenting.js',
            // path.projectFolder + '/scripts/star-rating.js',
            path.projectFolder + '/scripts/video.js',
            path.projectFolder + '/scripts/newsletter.js'

        ])
            .pipe(concat('projectteam.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var archive = gulp.src([
            path.projectFolder + '/scripts/_loadJSON.js',
            path.projectFolder + '/scripts/polyfill.js',
            path.projectFolder + '/scripts/detect.js',
            path.projectFolder + '/scripts/menu.js',
            path.projectFolder + '/scripts/newsletter.js'

        ])
            .pipe(concat('archive.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var search = gulp.src([
            './scripts/search/search-url-parameters.js',
            './scripts/search/search-checkboxes.js',
            './scripts/search/search-dropdown.js',
            './scripts/search/search-helpers.js',
            './scripts/search/search-filters.js',
            './scripts/search/search-pagination.js',
            './scripts/search/search-result-info.js',
            './scripts/search/search-result-content.js',
            './scripts/search/search-init.js'
        ])
            .pipe(concat('search.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var faq = gulp.src([
            './scripts/faq/search-url-parameters.js',
            // './scripts/search/search-checkboxes.js',
            // './scripts/search/search-dropdown.js',
            './scripts/faq/search-helpers.js',
            './scripts/faq/search-filters.js',
            './scripts/faq/search-pagination.js',
            './scripts/faq/search-result-info.js',
            './scripts/faq/search-result-content.js',
            './scripts/faq/search-init.js'
        ])
            .pipe(concat('faq.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));



        return merge(post, homepage, page, projectteam, archive, search, faq);

    });
}
