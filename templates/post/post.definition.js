const PagePersistence = require('../../../persistence/page.persistence');
const PathService = require('../../../services/path.service');
const logger = require('../../../services/logger.service');
const BooleanService = require('../../services/boolean.service');

const moment = require('moment');

module.exports = {

    searchSnippetTemplate: 'search-snippet', // filename of the handlebars template,

    /**
     * Gets the data for the search snippet
     * @param data
     */
    getSearchSnippetData: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve(data);
        })
    },

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {
        return new Promise((resolve, reject) => {

            let pagePersistence = new PagePersistence();

            let postOptions = {
                query : {
                    type:"post",
                    slug: { $ne : data.slug }
                },
                "sort": {"date":-1},
                "limit":4
            };
            let findPosts = pagePersistence.find(postOptions);

            Promise.all([findPosts]).then(values => {
                data.related_posts = values[0];
                resolve(data)
            });
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const date = moment(data.date, 'YYYY').format('YYYY');
            const pathService = new PathService();
            const path = 'nieuws/' + date + '/' + pathService.cleanString(data.slug);
            resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            // logger.info('Get template dependencies for ' + data.type, correlationId);
            resolve([
                {template: 'homepage', data: null},
                {template: 'search', data: null}
            ]);
        })
    },
    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: (html, path, data, correlationId) => {
        return new Promise((resolve, reject) => {
            // logger.info('Post-render function: ' + path, correlationId);
            resolve(html);
        })
    }
}
