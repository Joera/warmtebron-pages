const PagePersistence = require('../../../persistence/page.persistence');
const SocialPersistence = require('../../../persistence/social.persistence');
const PathService = require('../../../services/path.service');
const DiscussionService = require('../../services/discussion.service');
const logger = require('../../../services/logger.service');
const moment = require('moment');

module.exports = {


    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            let pagePersistence = new PagePersistence();
            let socialPersistence = new SocialPersistence();
            let discussionService = new DiscussionService();

            let homeOptions = {
                query : {
                    "slug":"homepage"
                }
            }

            let postOptions = {
                query : {
                    "type": "post",
                    "_id": { $nin : ['76'] },
                    "status": "publish"
                },
                "sort": {"date":-1},
                "limit": 12
            };

            let pageOptions = {
                query : {
                    "type":"page",
                    "slug": { $nin : ['homepage','zoek','contact'] }
                }
            };

            let contactOptions = {
                query : {
                    "type":"contact",
                    "featured_item": { $in : [true,1,'1']}
                }
            };

            let partnerOptions = {
                query : {
                    "type":"partner"
                }
            };

            let socialOptions = {
                query : {
                    publishDate : { $ne : null }
                },
                "limit": 3
            };

            let featuredOptions = {
                query : {
                    "type":"page",
                    "slug": { $in : ['risico-en-maatregelen'] }
                }
            };


            let findHome = pagePersistence.find(homeOptions);
            let findPosts = pagePersistence.find(postOptions);
            let findPages = pagePersistence.find(pageOptions);
            let findContacts = pagePersistence.find(contactOptions);
            let findPartners = pagePersistence.find(partnerOptions);
            let findFeatured = pagePersistence.find(featuredOptions);
            let findSocials = socialPersistence.find(socialOptions);
            let getDiscussion = discussionService.get();

			Promise.all([findHome,findPosts,findPages,findContacts,findPartners,findFeatured,getDiscussion,findSocials]).then(values => {

                // values[1] = values[1].concat(values[1]).concat(values[1]);

                let threads = [];
                values[6].forEach( (i) => {
                    i.interaction.nested_comments.forEach( (t) => {
                        t[0].link = i.url;
                        t[0].type = 'comment';
                        threads.push(t);
                    });
                });

			    data.home = values[0][0];
				data.pages = values[2];
                data.contacts = values[3];
                data.partners = values[4];
                data.posts = values[5]

                .concat(values[1].slice(0,2))
                .concat(values[7].slice(0,1))
                .concat(threads.slice(0,1))
                .concat(values[1].slice(2,4))
                .concat(values[7].slice(1,2))
                .concat(threads.slice(1,2))
                .concat(values[1].slice(4,6))
                .concat(values[7].slice(2,3))
                .concat(threads.slice(2,3))
                .concat(values[1].slice(6,8))
                ;

                logger.debug(data.posts);

				resolve(data)
			})
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, correlationId) => { // path generator for path/url of the page
        return '';
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve([]);
        })
    },


    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: (html, path, data, correlationId) => {
        return new Promise((resolve, reject) => {
            // logger.info('Post-render function: ' + path, correlationId);
            resolve(html);
        })
    }
}
