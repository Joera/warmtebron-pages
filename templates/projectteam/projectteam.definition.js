const PagePersistence = require('../../../persistence/page.persistence');
const PathService = require('../../../services/path.service');
const DiscussionService = require('../../services/discussion.service');

const logger = require('../../../services/logger.service');

const moment = require('moment');

module.exports = {

    /**
     * Gets the data for the search snippet
     * @param data
     */
    getSearchSnippetData: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            // logger.info('Get search snippet data', correlationId);
            resolve(data);
        })
    },

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            let pagePersistence = new PagePersistence();

            let options = {
                query : {
                    "slug":"projectteam"
                }
            }

            let contactOptions = {
                query : {
                    "type":"contact"
                }
            };

            let find = pagePersistence.find(options);
            let findContacts = pagePersistence.find(contactOptions);

			Promise.all([find,findContacts]).then(values => {

			    data.page = values[0][0];
                data.contacts = values[1];

				resolve(data)
			})
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const pathService = new PathService();
            const path = pathService.cleanString(data.slug);
            resolve(path);
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve([]);
        })

    },
    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: (html, path, data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve(html);
        })
    }
}
