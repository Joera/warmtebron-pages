
class Menu {

    constructor() {

        this.hamburger = document.querySelector('.icon-hamburger');
        this.menu = document.querySelector('#menu');
        this.nav = document.querySelector('nav');
        this.beeldmerk = document.querySelector('.beeldmerk');
        this.searchBar = document.getElementById('search-bar');
    }

    init() {

        let self = this;

        if (self.hamburger) {
            self.hamburger.addEventListener('click', function () {
                self.menu.classList.toggle('open');
            }, false);
        }

        setInterval( function() {

            self.beeldmerk.classList.add('equal-color');
            setTimeout( function() {
                self.beeldmerk.classList.remove('equal-color');
                self.beeldmerk.classList.add('reverse-color');
            },500);
            setTimeout( function() {
                self.beeldmerk.classList.remove('reverse-color');
                self.beeldmerk.classList.add('equal-color');
            },5000);
            setTimeout( function() {
                self.beeldmerk.classList.remove('equal-color');
            },5500);

        },24000);

    }

    onEnterSearch(e) {
        let self = this,
            key = e.which || e.keyCode;
        if(key == 13) {
            self.search();
        }
    }

    searchBarOpen() {

        let self = this;
        this.searchBar.focus();

        setTimeout( function() {
            self.searchBar.setAttribute('onclick','menu.search()');
        },200)
    }

    search() {

        window.location.href = '/nieuws/?query=' + this.searchBar.value + '&filter[type]=post,page&page=1';


    }
}

var menu = new Menu();
menu.init();