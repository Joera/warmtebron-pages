class PageViews {

    constructor() {

        this.stats = {};
        this.slug = null;
        this.el = document.querySelector('#pageview-count');
    }

    init(slug) {

        this.slug = slug;

        let self = this,
            url = '/sg-api/statistics?slug=' + self.slug;

        axios.get(url)
            .then(function(response){
                if (response.status !== 200) {
                    console.log('foutje bedankt')
                }

                console.log(response);

                self.el.innerText = ' | ' + response.data.pageviews + 'x gelezen';

            }).catch((error) => {
            console.log(error);
        });
    }
}