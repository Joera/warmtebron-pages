

class NewsletterSettings {

    constructor() {

        let self = this,
            getSubscriber = this.getSubscriber.bind(this),
            setSubscriberSettings = this.setSubscriberSettings.bind(this),
            unsubscribe = this.unsubscribe.bind(this);
        this.token = this.getParameterByName('newsletter-token');
        this.action = this.getParameterByName('action');
        this.subscriber = null;
        this.htmlDoc = document.getElementsByTagName('html')[0];
        this.unsubscribeUrl = null;

        this.header = document.querySelector('.newsletter-settings > .newsletter-settings-header > h1');
        this.unsubscribeMessage = document.querySelector('.newsletter-settings > .newsletter-settings-header > .newsletter-settings-email-unsubscribe');
        this.frequencyQuestion = document.querySelector('.newsletter-settings  .newsletter-settings-content-column:nth-child(1) p');
        this.frequencyOptions =  [].slice.call(document.querySelectorAll('.newsletter-settings .newsletter-settings-content-column:nth-child(1) form div'));
        this.preferencesColumn =  document.querySelector('.newsletter-settings .newsletter-settings-content-column:nth-child(2)');
        this.footer = document.querySelector('.newsletter-settings > .newsletter-settings-footer'); 

        // get subscriber and taxonomies data
        if (this.token !== null) {
            getSubscriber(setSubscriberSettings, this.token);
        }

        // unsubscribe
        if (this.action === 'unsubscribe') {
            getSubscriber(unsubscribe, this.token);
        }

    }

    // get subscriber info from remote host
    getSubscriber(succesCallback, token) {
        let self = this;
        let url = "/wp-json/wp/v2/subscriber?token=" + token,
            config = {};

        // sent http request for getting subscriber details
        axios.get(url, config)
            .then(function (response) {

                if(response.status === 200) {
                    // call back function if data is received ok (voor vervolg kijk boven in constructor)
                    // self.subscriber = response.data[0];
                    succesCallback(response.data[0]);
                }
            })
            .catch(function (error) {
                document.querySelector('.newsletter-settings > .newsletter-settings-content').innerHTML = 'Gegevens niet gevonden';
                self.showPopup();
            });
    }


    // get taxonomies from remote host
    getTaxonomies(subscriberData) {
        let self = this,
            url = "/wp-json/wp/v2/subscriptions",
            config = {};

        // sent http request for getting subscriber details
        axios.get(url, config)
            .then(function (response) {

                if(response.status === 200) {
                    self.renderTaxonomyCheckboxes('themes', 'Thema\'s', 'category', response.data.theme, subscriberData);
                    self.renderTaxonomyCheckboxes('construction-projects', 'Bouwprojecten', 'construction-project', response.data.constructionProject, subscriberData);
                    self.setTaxonomySettings(subscriberData);
                } else {
                    document.querySelector('.newsletter-settings > .newsletter-settings-content').innerHTML = 'Gegevens niet gevonden';
                }
            })
            .catch(function (error) {
                document.querySelector('.newsletter-settings > .newsletter-settings-content').innerHTML = 'Fout bij het ophalen van de gegevens';
                console.log(error);
            });
    }


    // update subscriber info
    onUpdateSubscriber() {

        // set the subscription value
        this.subscriber.subscription.type = document.querySelector('.newsletter-settings > .newsletter-settings-content input[name="subscription"]:checked').value;
        //
        let self = this,
            url = "/wp-json/wp/v2/subscriber",
            subject = document.querySelector('input[name=subject]:checked').value,
            language = self.subscriber.subscription.language || 'nl',
            themes = (this.getCheckedTaxonomies('category') === '' || subject === 'all') ? '' : this.getCheckedTaxonomies('category').join(),
            constructionProjects = (this.getCheckedTaxonomies('construction-project') === '' || subject === 'all') ? '' : this.getCheckedTaxonomies('construction-project').join(),
            body = 'name=-&id=' + this.subscriber.id + '&subscription=' + this.subscriber.subscription.type + '&email=' + this.subscriber.subscription.email + '&language=' + language + '&themes=' + themes + '&constructionprojects=' + constructionProjects,
            config = {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            };

        // make call
        axios.put(url, body, config)
            .then(function (response) {
                self.navigateHome();
            })
            .catch(function (error) {
                document.querySelector('.newsletter-settings > .newsletter-settings-content').innerText = 'Opslaan gegevens mislukt!';
                console.log(error);
            });
    }


    // unsubscribe
    unsubscribe(subscriber) {

        let self = this,
            url = "/wp-json/wp/v2/subscriber",
            body = 'name=-&id=' + subscriber.id + '&subscription=none&email=' + subscriber.email,
            config = {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            };

        self.footer.innerHTML = '<a href="http://warmtebron.nu">Sluiten</a>';

        document.querySelector('.newsletter-settings > .newsletter-settings-header > .newsletter-settings-email-unsubscribe').innerHTML = '';

        // make call
        axios.put(url, body, config)
            .then(function (response) {
                console.log(response);
                document.querySelector('.newsletter-settings > .newsletter-settings-content').innerHTML = 'Uitschrijven is gelukt.';
            })
            .catch(function (error) {
                document.querySelector('.newsletter-settings > .newsletter-settings-content').innerHTML = 'Uitschrijven mislukt!';
                console.log(error);
            });
    }


    // render checkboxes
    renderTaxonomyCheckboxes(elementId, heading, taxonomy, terms, subscriberData) {

        if (subscriberData && subscriberData.subscription.taxonomies && terms) { // check if all data is set to render the checkboxes
            let html = '<span>' + heading + '</span>';

            for (let i = 0; i < terms.length; i++) {
                    let checked = (this.isChecked(terms[i].term_id, subscriberData.subscription.taxonomies)) ? 'checked' : '';
                    html += '<div><input type="checkbox" name="' + taxonomy + '" id="' + terms[i].name + '"' + checked + '><label for="' + terms[i].term_id + '">' + terms[i].name + '</label></div>';
            }
            document.getElementById(elementId).innerHTML = html;
        }
    }


    // add subscriber data to DOM
    setSubscriberSettings(subscriber) {

        let self = this;

        console.log(subscriber);

        // let languageOptions = [].slice.call(document.querySelectorAll('ul.language-switch li'));
        //
        // let activeOption = languageOptions.find((o) => {
        //    return o.getAttribute('data-attr') == subscriber.subscription.language;
        // });
        //
        // languageOptions.forEach( (li) => {
        //     li.classList.remove('active');
        // });
        //
        // if (activeOption) {
        //     activeOption.classList.add('active');
        // }
        //
        // languageOptions.forEach( (li) => {
        //     li.addEventListener('click', function(o) {
        //         subscriber.subscription.language = li.getAttribute('data-attr');
        //         self.setSubscriberSettings(subscriber)
        //
        //     }, false)
        // });

       self.unsubscribeUrl = window.location.href + '&action=unsubscribe';
       document.querySelector('.newsletter-settings > .newsletter-settings-header > .newsletter-settings-email').innerHTML = subscriber.subscription.email; // set emailaddresself.unsubscribeMessage.innerHTML = 'Mijn e-mailadres <a href="' + self.unsubscribeUrl + '">uitschrijven</a>';
        self.unsubscribeMessage.innerHTML = 'Mijn e-mailadres <a href="' + self.unsubscribeUrl + '">uitschrijven</a>';

        //
        // if(subscriber.subscription.language === 'en') {
        //
        //    self.switchToEnglish();
        //
        // } else {
        //
        //     self.switchToDutch();
        // }

        self.showPopup(subscriber);

        // get taxonomy data from api
       // this.getTaxonomies(subscriber);
    }


    // check the select box for receiving all messages or only on specific topics based on if any taxonomy(topic) is set
    setTaxonomySettings(subscriberData) {
        if(subscriberData.subscription.taxonomies.length > 0) { // check if there are taxonomies set
            // taxonomies set
            document.getElementById('subject-specific').checked = true;
        } else {
            // no taxonomies set
            document.getElementById('subject-all').checked = true;
            this.onToggleTaxonomies('hide');
        }
    }


    // returns array of id's of the checked checkboxes with the name taxonomies
    getCheckedTaxonomies(taxonomy) {
        let checkboxes = document.getElementsByName(taxonomy);
        let checkboxesChecked = [];

        // loop checkboxes
        for (var i = 0; i < checkboxes.length; i++) {
            // add checked to checkboxesChecked array
            if (checkboxes[i].checked) {
                checkboxesChecked.push(checkboxes[i].id); // add id of selected taxonomy
            }
        }
        return checkboxesChecked.length > 0 ? checkboxesChecked : '';
    }


    // return true if object in collection has id value that is supplied in id
    isChecked(id, collection) {

        let returnValue = false;
        for(let i = 0; i < collection.length; i++) {
            if(returnValue === false) { // if not found yet
                returnValue = (String(collection[i].term_id) == String(id));
            }
        }
        return returnValue;
    }


    // show / hide the taxonomies select boxes in the UI
    onToggleTaxonomies(action) {
        if(action === 'show') {
            document.getElementById('taxonomy-options').className = 'multiselect';
        } else {
            document.getElementById('taxonomy-options').className = 'multiselect hidden';
        }
    }

    // get parameter from url
    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // navigate to the home page
    navigateHome() {
        this.htmlDoc.classList.remove('allabouthemap');

        let url = '/';

        if(subscriber.subscription.language === 'en') {
            url = '/english/';
        }

        window.location.href = url;
    }

    switchToEnglish() {

        let self = this;

        self.header.innerText = 'E-mail preferences';
        self.unsubscribeMessage.innerHTML = '<a href="' + self.unsubscribeUrl + '">Unsubscribe</a>'; // set unsubscribe link
        self.frequencyQuestion.innerText = 'How frequent do you want to hear from us?';

        self.frequencyOptions[0].querySelector('label span').innerText = "Every new article";
        self.frequencyOptions[1].querySelector('label span').innerText = "Weekly";
        self.frequencyOptions[2].querySelector('label span').innerText = "Monthly";

        self.preferencesColumn.style.display = 'none';

        self.footer.querySelector('button').innerText = 'Save';
        self.footer.querySelector('a').innerText = 'Abort';
    }

    switchToDutch() {

        let self = this;

        self.header.innerText = 'E-mailvoorkeuren';
        self.unsubscribeMessage.innerHTML = 'Mijn e-mailadres <a href="' + self.unsubscribeUrl + '">uitschrijven</a>';

        self.frequencyQuestion.innerText = 'Hoe vaak wil je van ons horen?';

        self.frequencyOptions[0].querySelector('label span').innerText = "Bij ieder nieuw artikel";
        self.frequencyOptions[1].querySelector('label span').innerText = "Wekelijks";
        self.frequencyOptions[2].querySelector('label span').innerText = "Maandelijks";

        self.preferencesColumn.style.display = 'block';

        self.footer.querySelector('button').innerText = 'Opslaan';
        self.footer.querySelector('a').innerText = 'Annuleren';
    }

    showPopup(subscriber) {

        document.querySelector('.newsletter-settings-container').classList.add('show');
      //  document.querySelector('.newsletter-settings > .newsletter-settings-content input[value=' + subscriber.subscription.type + ']').checked = true; // set selected option
        this.htmlDoc.classList.add('allabouthemap');
    }




}

let newsletterSettings = new NewsletterSettings();