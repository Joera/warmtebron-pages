var SearchUrlParameters = function SearchUrlParameters(config,parameters,elements) {

    var urlParameters = parameters;

    var get = function get() {

        urlParameters.query = getParameterByName('query') || '';

        if (urlParameters.query) {
            elements.searchBox.value = urlParameters.query;
        }

        let page = getParameterByName('page') || 0;

        if (page) {
            urlParameters.pageIndex = parseInt(page) - 1;
        }


        if (getParameterByName('error')) {
            let error = getParameterByName('error').replace(/^\/|\/$/g, '');
            urlParameters.query = error;
            elements.searchBox.value = error;
        }

        return urlParameters;
    }

    var set = function set(parameters) {

        urlParameters = parameters;
        pushState();

    }

    var getParameterByName = function getParameterByName(name, url) {

        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }


    var getFilters = function getFilters(filter) {

        let theme = getParameterByName('filter[theme]') || false;

        // set search term in search input box when coming from other page
        if (theme) {
            filter.themes = [theme];
        }

        let tag = getParameterByName('filter[tag]') || false;

        // set search term in search input box when coming from other page
        if (tag) {
            filter.tags = [tag];
        }

        let constructionProject = getParameterByName('filter[constructionProject]') || false;

        // set search term in search input box when coming from other page
        if (constructionProject) {
            filter.constructionProjects = [constructionProject];
        }

        let types = getParameterByName('filter[type]') || false;

        if(types) {

            let typeArray = types.split(',');
            filter.types = [];
            typeArray.forEach ((t) => {
                filter.types.push({ id : t, state : true});
            });
        }

        return filter;
    };



    // var pushState = function pushState() {
    //
    //     let params = Object.keys(urlParameters.filter).map(function(k) {
    //
    //         if(filter[k] != null && filter[k].length > 0) {
    //             if(Array.isArray(filter[k])) {
    //
    //                 filter[k].forEach( f => {
    //                     // doe iets man!
    //                 });
    //
    //             } else {
    //                 return encodeURIComponent(k) + '=' + encodeURIComponent(filter[k]);
    //             }
    //         }
    //     }).join('&');
    //
    //     if (history.pushState) {
    //         var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + params
    //         window.history.pushState({path:newurl},'',newurl);
    //     }
    // }

    var pushState = function pushState() {

        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;

        if (history.pushState) {

            newurl = newurl + '?';

            if (urlParameters.query != '') {

                newurl = newurl + 'query=' + urlParameters.query;
                newurl = newurl + '&';
            }

            if (urlParameters.filter.types.length > 0) {

                newurl = newurl + 'filter[type]=';
                let x = 0;

                for (let i = 0; i < urlParameters.filter.types.length; i++)  {

                    if(x > 0 && urlParameters.filter.types[i].state == true) {
                        newurl = newurl + ',';
                    }
                    if(urlParameters.filter.types[i].state == true) {
                        newurl = newurl + urlParameters.filter.types[i].id;
                        x++;
                    }
                }
                newurl = newurl + '&';
            }

            if (urlParameters.filter.themes.length > 0) {

                newurl = newurl + 'filter[theme]=';
                let x = 0;

                for (let i = 0; i < urlParameters.filter.themes.length; i++)  {

                    console.log(urlParameters.filter.themes);

                    if(x > 0) {
                        newurl = newurl + ',';
                    }

                    newurl = newurl + urlParameters.filter.themes[i];
                    x++;

                }
                newurl = newurl + '&';
            }

            if (urlParameters.filter.tags.length > 0) {

                newurl = newurl + 'filter[tag]=';
                let x = 0;

                for (let i = 0; i < urlParameters.filter.tags.length; i++)  {

                    if(x > 0) {
                        newurl = newurl + ',';
                    }

                    newurl = newurl + urlParameters.filter.tags[i];
                    x++;

                }
                newurl = newurl + '&';
            }

            if (urlParameters.filter.constructionProjects.length > 0) {

                newurl = newurl + 'filter[constructionProjects]=';
                let x = 0;

                for (let i = 0; i < urlParameters.filter.constructionProjects.length; i++)  {

                    if(x > 0) {
                        newurl = newurl + ',';
                    }

                    newurl = newurl + urlParameters.filter.constructionProjects[i];
                    x++;

                }
                newurl = newurl + '&';
            }

            newurl = newurl + 'page=' + (parseInt(urlParameters.pageIndex) + 1);

            window.history.pushState({path:newurl},'',newurl);
        }
    }


    return {

        get: get,
        set: set,
        getParameterByName : getParameterByName,
        getFilters : getFilters
    };
}

