var SearchDropdown = function SearchDropdown() {

    var setListeners = function setListeners(container) {

        let self = this,
            choices = [].slice.call(container.querySelectorAll('li'));

        container.querySelector('#content-filter-toggle-button').addEventListener("click", function(e){ openContentFilter(e,container) }, false);

        choices.forEach( function(li,i) {
            li.addEventListener('click', function() { _actOnDropdown(choices[i],container) }, false);
        });
    }

    var _actOnDropdown = function _actOnDropdown(li,container) {

        _setDropdown(li);
        closeContentFilter(container);
        // searchInit.filter.types = searchInit.setTypes(li.getAttribute('id'));
        // searchInit.getSearch(searchInit.currentIndex,searchInit.filter,searchInit.query,0);
        // searchInit.pushState(li.getAttribute('id'));
    }

    var _setDropdown = function _setDropdown(li) {

        [].slice.call(document.querySelectorAll('li')).forEach( function(l,i) {

            if (l.classList.contains('active')) {
                l.classList.remove('active');
            }

            if (li.getAttribute('id') === l.getAttribute('id') &&  !l.classList.contains('active')) {
                l.classList.add('active');
            }
        });
    }

    var openContentFilter = function openContentFilter(e,container) {
        container.classList.add("open");
    }

    var closeContentFilter = function closeContentFilter(container) {
        container.classList.remove("open");
    }

    return {
        setListeners : setListeners,
        openContentFilter: openContentFilter,
        closeContentFilter: closeContentFilter
    };
}