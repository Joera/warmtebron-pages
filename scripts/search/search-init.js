
// beter onderscheid maken tussen 'opdracht' (=query)  en de filter (gebeurt erna)
// wellicht een groot parameters object
// parameters.query
// parameters.page
// parameters.filters

var Search = function Search(language) {

    var config = {

        app_id : '37QFMTU4MB',
        api_key : '75131e2b879a683f6d30fe318f468436',
        index : 'all_warmtebron',
        available_types : ['post','page','comment', 'document',]
    }

    var elements = {

        content : [].slice.call(document.querySelectorAll('#archived-content a')),
        contentContainer : document.querySelector('#search-content-container'),
        submitButton : document.querySelector('#search-form button'),
        searchBox : document.querySelector('#archive-search-box'),
        resultCountContainer : document.querySelector('#result-count-container'),
        paginationContainer : document.querySelector('#pagination'),
        contentFilter : document.querySelector('#content-filter'),
        tagFilterContainer : document.querySelector('#tag-container'),
       // projectFilterContainer : document.querySelector('#projects-container'),
        interactionContainer : document.querySelector('aside')

    }

    var parameters = {

        'query' : '',
        'pageIndex' : 0,
        'filter' : []
    };

    const searchHelpers = SearchHelpers(config,elements);
    const searchUrlParameters = SearchUrlParameters(config,parameters,elements);
    const searchResultContent = SearchResultContent();
    const searchResultInfo = SearchResultInfo(language);
    const searchPagination = SearchPagination();
    const searchCheckboxes = SearchCheckboxes(elements);
    const searchFilters = SearchFilters(config,searchUrlParameters,language);

    let index = searchHelpers.initIndex({});
    parameters = searchUrlParameters.get();
    parameters.filter = searchFilters.initFilter({},parameters.query,true);
    parameters.filter = searchUrlParameters.getFilters(parameters.filter);
    searchCheckboxes.setCheckboxes(parameters.filter);


    let getSearch = function getSearch(pageIndex) {

        if(pageIndex || pageIndex == 0) {
            parameters.pageIndex = pageIndex;
        }
        
        let formattedFilter = searchFilters.concatString(parameters.filter,language);

        console.log(formattedFilter);


        index.search(parameters.query, {
            facets: '*',
            filters: formattedFilter,   // checked
            attributesToRetrieve: ['title','content','sections','comment_author','comment_content','comment_count','comment_children','searchSnippet'],
            page: parameters.pageIndex,
            hitsPerPage: 16

        }, function searchDone(err, content) {

            console.log(content);

            searchResultContent.addHTML(content,elements.contentContainer);
            searchResultInfo.addHTML(content, parameters.pageIndex, elements.resultCountContainer);
            searchPagination.addHTML(index,content,parameters.pageIndex,elements.paginationContainer);
            searchFilters.addHTML(content,elements,parameters);
            searchCheckboxes.setCheckboxes(parameters.filter);
            searchUrlParameters.set(parameters);

        });
    }

    //query initial results
    getSearch(0);

    let onQueryChange = function onChange() {
        parameters.query = elements.searchBox.value;
        parameters.pageIndex = 0;
        parameters.filter = searchFilters.initFilter({},parameters.query,false);
        getSearch();
    }

    let onFilterChange = function onFilterChange(type,args) {

        if(type === 'types') {
            parameters.filter.types = args;
        } else if (type === 'taxonomies.categories.slug' && parameters.filter.themes.indexOf(args) < 0) {
            parameters.filter.themes.push(args);
        } else if (type === 'taxonomies.tags.slug' && parameters.filter.tags.indexOf(args) < 0) {
            parameters.filter.tags.push(args);
        } else if (type === 'taxonomies.construction_projects.slug' && parameters.filter.constructionProjects.indexOf(args) < 0) {
            parameters.filter.constructionProjects.push(args);
        }

       // searchHelpers.pushState(filter)
        getSearch(0);
    }

    let clearFilter = function clearFilter() {
        parameters.filter = searchFilters.initFilter({},false);
        getSearch(0);
    }

    let openFilters = function openFilters() {
        elements.interactionContainer.classList.add('show-on-portrait');
    }

    let closeFilters = function closeFilters() {
        elements.interactionContainer.classList.remove('show-on-portrait');
    }

    let showMoreTags = function showMoreTags() {
        elements.tagFilterContainer.classList.add('show-all');
    }

    // let showMoreProjects = function showMoreProjects() {
    //     elements.projectFilterContainer.classList.add('show-all');
    // }


    // interaction
    searchHelpers.initButton();
    searchCheckboxes.setListeners(parameters.filter);

    window.onpopstate = function(event) {
        console.log(event);
        parameters = searchUrlParameters.get();
        console.log(parameters);
        getSearch(parameters.pageIndex);

    };


    return {
        getSearch : getSearch,
        onQueryChange : onQueryChange,
        onFilterChange : onFilterChange,
        clearFilter : clearFilter,
        openFilters : openFilters,
        closeFilters : closeFilters,
        showMoreTags : showMoreTags,
        // showMoreProjects : showMoreProjects
    }
}



