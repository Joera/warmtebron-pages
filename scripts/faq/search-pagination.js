var SearchPagination = function SearchPagination() {

    var addHTML = function addHTML(index,content,pageIndex,container) {

        let self = this,
            firstInView,
            li,
            lastInView;

        // add pagination



        container.innerHTML = '';

        if (content.nbPages > 1) {

            if (pageIndex < 3) {
                firstInView = 1;
                lastInView = pageIndex + 4;
            }
            else if (pageIndex === 3) {
                firstInView = 1;
                lastInView = 6;
            } else if (pageIndex === 4) {
                firstInView = 2;
                lastInView = 7;
            }
            else {
                firstInView = pageIndex - 1;
                lastInView = pageIndex + 2;
            }

            // items links

            if (pageIndex > 0 && pageIndex <= content.nbPages) {

                let previous = document.createElement('li');
                previous.innerHTML = '<';
                previous.onclick = function () {

                    search.getSearch(pageIndex - 1)
                };
                container.appendChild(previous);
            }

            if (pageIndex > 3 && pageIndex <= content.nbPages) {

                let first = document.createElement('li');
                first.innerHTML = '1';
                first.onclick = function () {

                    search.getSearch(0)
                };
                container.appendChild(first);
            }

            if (pageIndex > 4 && pageIndex <= content.nbPages) {


                let points = document.createElement('li');
                points.innerHTML = '...';
                points.classList.add('points');
                container.appendChild(points);

            }

            // items active
            for (let i = firstInView; i < lastInView; i++) {
                if (i <= content.nbPages) {
                    li = document.createElement('li');
                    if (i === parseInt(pageIndex + 1)) {
                        li.classList.add('active');
                    }
                    li.innerHTML = i;
                    li.onclick = function () {
                        search.getSearch(i - 1);
                    }
                    container.appendChild(li);
                }
            }
            // items rechts
            if (content.nbPages > 4 && pageIndex <= content.nbPages - 2) {

                let points = document.createElement('li');
                points.innerHTML = '...';
                points.classList.add('points');
                container.appendChild(points);

                let last = document.createElement('li');
                last.innerHTML = content.nbPages;
                last.onclick = function () {

                    search.getSearch(content.nbPages - 1)
                };
                container.appendChild(last);

            }

            if (content.nbPages > 3 && pageIndex <= content.nbPages - 1) {


                let next = document.createElement('li');
                next.innerHTML = '>';
                next.onclick = function () {
                    console.log('5');
                    search.getSearch(pageIndex + 1)
                };
                container.appendChild(next);

            }
        }
    }

    return {
        addHTML : addHTML
    };
}