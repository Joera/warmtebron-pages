
var SearchCheckboxes = function SearchCheckboxes(elements) {

    let checkboxes = [].slice.call(elements.contentFilter.querySelectorAll('li > div > input[type="checkbox"]'));

    var setListeners = function setListeners(container,filterObject) {

        checkboxes.forEach( function(box,i) {
            box.addEventListener('click', function(e) {
                e.stopPropagation();
                search.onFilterChange('types',_readCheckboxes())

            }, false);
        });
    }

    var _readCheckboxes = function _readCheckboxes() {

        let types = [];
        checkboxes.forEach( function(el,i) {
            types.push({
                    id: el.id,
                    state: el.checked
                });
        });

        return types;
    }


    var setCheckboxes = function _setCheckboxes(filter) {
        
        let array = filter.types.map(
            function(t) {
                if(t.state) {
                    return t.id;
                }
            }
        );
        
        checkboxes.forEach( (el) => {
            if(array.indexOf(el.id) > -1) {
                el.checked = true;
            } else {
                el.checked = false;
            }
        });
    }

    var _initCheckboxes = function _initCheckboxes() {

        types.forEach( (el) => {

            el.checked = true;
            let item = {
                id: el.id,
                state: true
            };
            this.filter.types.push(item)
        });
    }

    var _ObjectToArray = function _ObjectToArray(object) {

        let array = [],
            v;

        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                let v = {
                    'name' : key,
                    'count' : object[key]
                }
                array.push(v);
            }
        }
        return array;
    }

    return {
        setListeners : setListeners,
        setCheckboxes : setCheckboxes
    };

}