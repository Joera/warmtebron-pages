var SearchFilters = function SearchFilters(config,searchUrlParameters,language) {


    let filteredByTag = false;


    var _ObjectToArray = function _ObjectToArray(object) {

        let array = [],
            v;

        for (var key in object) {
            if (object.hasOwnProperty(key)) { //  && key !== 'Statische Content'
                let v = {
                    'name' : key,
                    'count' : object[key]
                }
                array.push(v);
            }
        }
        return array;
    }


    var setTypes = function setTypes(type) {

        let self = this,
            types;

        if (type === 'all' || type === null || type === undefined ) {
            types = _setAllTypes();
        } else {
            types = [{ id: type, state: true }];
        }
        return types;
    }

    var _setAllTypes = function _setAllTypes() {

        let types = [];
        config.available_types.forEach( function(type) {
            types.push({
                id: type,
                state: true
            })
        });
        return types;
    }

    var initFilter = function initFilter(filter,query) {

        filter.tags = [config.tag];
        filter.types = readUrlContentParameters([],query);

        return filter;
    }

    var readUrlContentParameters = function readUrlContentParameters(types,query) {

       // console.log(query)

        if(query == '') {
            types = [{
                id: 'faq',
                state: true
            }]
        } else {
            types = _setAllTypes();
        }


        return types;
    }

    var concatString = function concatString(filter,language) {

        let self = this,
            i,
            facetFilter = '';

        facetFilter = facetFilter.concat('language.code:' + language) ;

        filter.tags.forEach(function(tag) {

            if (facetFilter !== '') {
                facetFilter = facetFilter.concat(' AND ')
            }
            facetFilter = facetFilter.concat('tag:' + tag);
            self.filteredByTag = true;
        });

        let trueTypeCount = 0;

        for (i = 0; i < filter.types.length; i++) {

            if(filter.types[i].state) {
                trueTypeCount++;
            }

            if (facetFilter !== '' && i == 0) {
                facetFilter = facetFilter.concat(' AND ');
            }

            if (i == 0 && filter.types.length > 1) {
                facetFilter = facetFilter.concat('(');
            }

            if (facetFilter !== '' && filter.types[i].state === true && i > 0 && trueTypeCount > 1) {
                facetFilter = facetFilter.concat(' OR ');
            }

            if(filter.types[i].state === true) {
                facetFilter = facetFilter.concat('type:' + filter.types[i].id);
            }

            if(filter.types.length > 1 && (filter.types.length - 1) == i  ) {
                facetFilter = facetFilter.concat(')');
            }

        }

        return facetFilter;
    }

    var addHTML = function addHTML(content,elements,parameters) {

        let self = this,
            tag_li,
            theme_li,
            project_li,
            themeList = [],
            tagList = [],
            projectList = [],
            tagSlugs = [],
            themeSlugs = [],
            projectSlugs = [],
            combinedList = [];

        elements.tagFilterContainer.innerHTML = '';
     //   elements.projectFilterContainer.innerHTML = '';

        if (content.facets['taxonomies.categories.name']) {
            themeList = _ObjectToArray(content.facets['taxonomies.categories.name']);
        }
        if (content.facets['taxonomies.categories.slug']) {
            themeSlugs = _ObjectToArray(content.facets['taxonomies.categories.slug']);
        }
        if (content.facets['taxonomies.tags.name']) {
            tagList = _ObjectToArray(content.facets['taxonomies.tags.name']);
        }
        if (content.facets['taxonomies.tags.slug']) {
            tagSlugs = _ObjectToArray(content.facets['taxonomies.tags.slug']);
        }
        if (content.facets['taxonomies.construction_projects.name']) {
            projectList = _ObjectToArray(content.facets['taxonomies.construction_projects.name']);
        }
        if (content.facets['taxonomies.construction_projects.slug']) {
            projectSlugs = _ObjectToArray(content.facets['taxonomies.construction_projects.slug']);
        }


        for (let a = 0; a < themeList.length; a++) {
            themeList[a]['slug'] = themeSlugs[a]['name'];
        }

        for (let i = 0; i < tagList.length; i++) {
            tagList[i]['slug'] = tagSlugs[i]['name'];
        }

        let NameAndSlugArray = [

            {'EMA' : 'ema'},
            {'Bicycle park Strawinskylaan' : 'strawinskylaan'},
            {'De Boelelaan Oost' : 'de-boelelaan-oost'},
            {'WTC' :'wtc'},
            {'2Amsterdam' : '2amsterdam'},
            {'Goede Doelen Loterijen' : 'goede-doelen-loterijen'},
            {'Imaging Center VUmc' : 'imaging-center-vumc'},
            {'Nhow Amsterdam RAI' :'nhow'},
            {'Sud' :'sud'},
            {'Suitsupply' :'suitsupply'},
            {'Valley' :'valley'},
            {'Van der Valk' :'nhow'},
            {'Nhow Amsterdam RAI' :'van-der-valk'},
            {'Xavier' :'xavier'}
        ];

        for (let i = 0; i < projectList.length; i++) {

            let match = NameAndSlugArray.find ( (pair) => {
                if (Object.keys(pair)[0] === projectList[i]['name']) {
                    // Object.values(pair) werkt vanaf node 7.0
                    return pair;
                }
            });

            if (match) {

                console.log(match);

                projectList[i]['slug'] = Object.values(match)[0];
            }
        }

        console.log(projectList);

        themeList.forEach( function(item) {

            theme_li = document.createElement('span');
            theme_li .classList.add('tag-tab','smallerfont');
            theme_li .innerHTML = item.name + '<span class="count evensmallerfont">(' + item.count + ')</span>';
            theme_li .onclick = function() { search.onFilterChange('taxonomies.categories.slug',item.slug); filteredByTag = true; };
            elements.tagFilterContainer.appendChild(theme_li);
        });

        // add tags

        for (let i = 0; i < tagList.length; i++) {

            tag_li = document.createElement('span');
            tag_li.classList.add('tag-tab','smallerfont');
            if (i > 2) { tag_li.classList.add('initially_hidden'); }
            tag_li.innerHTML = tagList[i].name + '<span class="count evensmallerfont">(' + tagList[i].count + ')</span>';
            tag_li.onclick = function() { search.onFilterChange('taxonomies.tags.slug',tagList[i].slug); filteredByTag = true; };
            elements.tagFilterContainer.appendChild(tag_li);
            if (i === 2) {
                let divider = document.createElement('span');
                divider.classList.add('tag-tab','divider','smallerfont');
                divider.onclick = function() { search.showMoreTags()};
                divider.innerHTML = 'toon meer onderwerpen';
                elements.tagFilterContainer.appendChild(divider);
            }
        }

        if (parameters.filter.themes.length > 0 || parameters.filter.tags.length > 0) {

            let clearFilter = document.createElement('span');
            clearFilter.classList.add('tag-tab','clear','smallerfont');
            clearFilter.innerHTML = (language === 'en') ? 'remove filter' : 'filter verwijderen';
            clearFilter.onclick = function () {
                search.clearFilter();
                filteredByTag = false;
            };
            elements.tagFilterContainer.appendChild(clearFilter);

        }

        // for (let i = 0; i < projectList.length; i++) {
        //
        //     project_li = document.createElement('span');
        //     project_li.classList.add('tag-tab','smallerfont');
        //     if (i > 2) { project_li.classList.add('initially_hidden'); }
        //     project_li.innerHTML = projectList[i].name + '<span class="count evensmallerfont">(' + projectList[i].count + ')</span>';
        //     project_li.onclick = function() { search.onFilterChange('taxonomies.construction_projects.slug',projectList[i].slug);  };
        //     elements.projectFilterContainer.appendChild(project_li);
        //     if (i === 2) {
        //         let divider = document.createElement('span');
        //         divider.classList.add('tag-tab','divider','smallerfont');
        //         divider.onclick = function() { search.showMoreProjects()};
        //         divider.innerHTML = 'toon meer bouwprojecten';
        //         elements.projectFilterContainer.appendChild(divider);
        //     }
        // }

        // if (parameters.filter.constructionProjects.length > 0 || parameters.filter.constructionProjects.length > 0) {
        //
        //     let clearFilter = document.createElement('span');
        //     clearFilter.classList.add('tag-tab','clear','smallerfont');
        //     clearFilter.innerHTML = (language === 'en') ? 'remove filter' : 'filter verwijderen';
        //     clearFilter.onclick = function () {
        //         search.clearFilter();
        //         filteredByTag = false;
        //     };
        //     elements.projectFilterContainer.appendChild(clearFilter);
        //
        // }
    }

    return {

        initFilter: initFilter,
        concatString : concatString,
        setTypes: setTypes,
        addHTML : addHTML
    };
}
