
class Newsletter {

    constructor(){

        let self = this;

        this.buttons = [].slice.call(document.querySelectorAll('.subscription-form button'));

        for (let button of this.buttons) {

            button.addEventListener('click', function(event) { self.subscribe(event,'nl'); }, false);
        }
    }


    subscribeError(event,message) {
        let messageElement = event.target.parentNode.parentNode.querySelector('.subscription-notification');

        messageElement.innerText = message;
        messageElement.classList.add('visible');
    }

    subscribe(event,language) {

        if (!language || language === undefined) {
            language = 'nl';
        }

        let self = this,
            url = "/wp-json/wp/v2/subscriber/",
//                body = {'name': '-', 'email': document.querySelector('#footer-subscribe > input').value, 'subscription': 'direct'},
            email = event.target.parentNode.parentNode.querySelector('input').value,
            body = 'name=-&subscription=direct&email=' + email + '&language=' + language,
            config = {
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            };

        console.log(email);

        if (email && email !== '') { // check if email address is filled out
            // sent http request to subscribe to newsletter
            axios.post(url, body, config)
                .then(function (response) {

                    console.log(response);

                    if (response.data.status === 'error' && response.data.error === 'Already subscribed') {
                        self.subscribeError(event,'U bent aangemeld voor de nieuwsbrief');
                    } else if (response.data.status === 'error' && response.data.error === 'Please enter a valid email address.') {
                        // invalid email address
                        self.subscribeError(event,'Vul een geldig email adres in');
                    } else if (response.data.status === 'error') {
                        // other error
                        self.subscribeError(event,'Er is iets fout gegaan bij het aanmelden voor de nieuwsbrief');
                    } else {
                        self.subscribeError(event,'U bent aangemeld voor de nieuwsbrief');
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    self.subscribeError(event,'Er is iets fout gegaan bij het aanmelden voor de nieuwsbrief');
                });
        }
    }
}

let newsletter = new Newsletter();
