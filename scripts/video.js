class Video {

    constructor(){}

    play(button) {

        var videoContainer = button.parentNode,
            children = videoContainer.childNodes,
            number_of_children = children.length;

        for (var i=0; i<number_of_children; i++) {

            if (children[i] instanceof HTMLImageElement) {

                var image = children[i];
                image.style.display = "none";
                button.style.display = "none";

            } else if (children[i] instanceof HTMLIFrameElement){

                var iframe = children[i];
                iframe.style.display = "block";

                var src = iframe.src;
                iframe.src = src + '?autoplay=1&mute=1';
            }
        }
    }
}

var video = new Video;



