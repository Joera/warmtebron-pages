class StarRating {

    constructor() {

        this.rating = {};
        this.post_id = null;
    }

    init(post_id) {

        let self = this;

        self.url = window.location.href;
        self.post_id = post_id;
        self.rating = null;

        this.stars = [].slice.call(document.querySelectorAll('#appreciation-row svg'));
        this.button = document.querySelector('#blog-rating button');
        this.score = document.querySelector('#blog-rating span.score');
        this.message = document.querySelector('#blog-rating div.message');

        if(this.button) {

            this.button.addEventListener('click', function handlerRate() {
                self.rate();
                self.button.removeEventListener('click', handlerRate, true);
            }, true);

            this.get();
        }

        if (!self.isRated(this.url)) {

            this.stars.forEach((s, i) => {
                s.addEventListener('click', function handlerRate() {
                    self._colour(i);
                    self.rating = i;
                    s.removeEventListener('click', handlerRate, true);
                }, true);
            });
        }

        if(self.isRated(this.url)) {
           self.disableRatingButtons(); // disable rating buttons
        }
    }

    _colour(i) {

        this.stars.forEach( (s) => {
            s.classList.remove('active');
        });

        for ( let j = 0; j <= i; j++) {

            this.stars[j].classList.add('active');
        }
    }

    get() {

        let self = this,
            url = '/sg-api/ratings?post_ID=' + self.post_id;

        axios.get(url)
        .then(function(response){
            if (response.status !== 200) {
                console.log('foutje bedankt')
            }

            self.setScore(response.data);
            if(self.isRated(self.url)) {
                self._colour(Math.floor(response.data.score) - 1); // disable rating buttons
            }

        }).catch((error) => {
            console.log(error);
        });
    }

    rate() {

            let self = this,
                url = '/sg-api/ratings';

            axios.post(url, {

                    'post_ID' : self.post_id,
                    'value' : parseInt(self.rating) + 1
            })
            .then(function (response) {
                if (response.status !== 200) {
                    console.log('foutje bedankt')
                }
                self.setRated(self.url);
                self.setScore(response.data);
                self._colour(Math.floor(response.data.score) - 1);
            });
    }

    setScore(data) {

        this.score.innerText = '(' + data.score + '/' +  data.total + ')';

    }

    setRated(url) {
        let self = this;
        if (typeof(Storage) !== 'undefined') { // check if local stage is supported by browser

            let ratedPages = null;

            if (localStorage.getItem('ratedPages')) {
                ratedPages = JSON.parse(localStorage.getItem('ratedPages')); // get ratedPages array from local storage
            }

            // check if local storage property exists
            if(ratedPages === null) {
                ratedPages = [];
            }
            ratedPages.push(url); // add page to ratedPages

            localStorage.setItem('ratedPages', JSON.stringify(ratedPages)); // store in local storage
            self.disableRatingButtons(); // disable rating buttons
        } else {
            // No local storage support
        }
    }

    isRated(url) {

        let ratedPages = null;

        if (localStorage.getItem('ratedPages')) {
            ratedPages = JSON.parse(localStorage.getItem('ratedPages')) || []; // get ratedPages array from local storage
        }

        if(ratedPages !== null && ratedPages.indexOf(url) !== -1) { // check if local storage property exists
            return true;
        }
        return false;
    }

    disableRatingButtons() {

        let self = this;
        self.button.style.display = 'none';
        self.message.innerText = 'Je hebt gestemd voor dit bericht.'
    }
}