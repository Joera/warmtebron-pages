
class Header {

    constructor() {

        this.body = document.getElementsByTagName('body')[0];
        this.articles = [].slice.call(document.querySelectorAll('aside .block'));
        this.header = document.getElementsByTagName('header')[0];
        this.main = document.getElementsByTagName('main')[0];
        this.aside = document.getElementsByTagName('aside')[0];
        this.commentSection = document.getElementById('comment-section');
    }

    init() {

        let self = this;

        window.onbeforeunload = function () {
            window.scrollTo(0, 0);
        }

        window.addEventListener('scroll', function(event) {
           if(parseInt(window.scrollY) < 1) {

               if (window.innerWidth > 900) {
                    self._reset();
               }
           }
        }, true);

            self._initWaypoints();
    }


    _initWaypoints() {

        let self = this;

        this.articles.forEach( (a,i) => {

                new Waypoint({
                    element: a,
                    handler: function (direction) {
                        if (direction === 'down') {

                            if (i == 1) {
                                // self._shrink(a,i);
                            } else if (i > 1){
                                self._inflate(a, i);
                            }
                        }
                    },
                    offset: 580
                });

                new Waypoint({
                    element: a,
                    handler: function (direction) {
                        if (direction === 'down') {

                            if (i == 1){
                                self._shrink(a, i);
                            }
                        }
                    },
                    offset: 100
                });
        });
    }

    _shrink(el,i) {

        let self = this, yStart, yCurrent, yRelevant, yOffset, opacityOffset, triggerOffset, text, textAside, gradient, w = 500, h = 333;

        text = el.querySelector('.text');
        textAside = el.querySelector('.text-aside');
        gradient = el.querySelector('.gradient');
        triggerOffset = 333;

        function shrinkOnScroll() {

            yCurrent = parseInt(window.scrollY);
            yRelevant = yCurrent - yStart;
            yOffset = 1 - (yRelevant / triggerOffset);
            opacityOffset = 1 - ((2 * yRelevant) / triggerOffset);

            if (yOffset > 0.4 && yOffset < 1) {

                el.style.width = "calc(" + w + "px * " + yOffset + ")";
                el.style.height = "calc(" + h + "px * " + yOffset + ")";
                text.style.opacity = opacityOffset;
                // textAside.style.opacity = 1 - opacityOffset;
                if  (gradient) {
                    gradient.style.opacity = (1 - opacityOffset);
                }

            }
        }

        if (window.innerWidth > 900) {
            yStart = parseInt(window.scrollY);
            window.addEventListener("scroll", shrinkOnScroll);
        }
    }


    _inflate(el,i) {

        let self = this, yCurrent, yStart, yRelevant, yOffset, opacityOffset, triggerOffset, text, textAside, gradient, w = 500, h = 333;

        text = el.querySelector('.text');
        textAside = el.querySelector('.text-aside');
        gradient = el.querySelector('.gradient');
        let once = true;
        triggerOffset = 333;

        function inflateOnScroll() {

            yCurrent = parseInt(window.scrollY);
            yRelevant = yCurrent - yStart;

            if (yRelevant < triggerOffset) {

                yOffset = 0.4 + (yRelevant / triggerOffset);
                opacityOffset = (2 * yRelevant) / triggerOffset;

                if (opacityOffset <  .25) {
                    opacityOffset = 0;
                } else if (opacityOffset >  .75) {
                    opacityOffset = 1;
                }

                if (yOffset > 0.4 && yOffset < 1) {

                    el.style.width = "calc(" + w + "px * " + yOffset + ")";
                    el.style.height = "calc(" + h + "px * " + yOffset + ")";
                    text.style.opacity = opacityOffset;
                    if(textAside) {
                        textAside.style.opacity = 1 - opacityOffset;
                    }
                    if(gradient) {
                        gradient.style.opacity = (1 - opacityOffset) * .5;
                    }
                }

            } else {

                if (once) {
                    self._shrink(el,i);
                    once = false;
                }

            }
        }

        if (window.innerWidth > 900) {
            yStart = parseInt(window.scrollY);
            window.addEventListener("scroll", inflateOnScroll);
        }
    }

    _reset () {

        console.log('reset');
        // window.removeEventListener("scroll",self._inflateOnScroll);


        let text, textAside, gradient;

        this.articles.forEach( (el) =>{

            text = el.querySelector('.text');
            textAside = el.querySelector('.text-aside');
            gradient = el.querySelector('.gradient');

            el.style.width = '200px';
            el.style.height = '133px';
            text.style.opacity = 0;
            if(textAside){
                textAside.style.opacity = 1;
            }
            if(gradient) {
                gradient.style.opacity = .5;
            }

        });

        this.articles[0].style.width = '300px';
        this.articles[0].style.height = '200px';

        this.articles[1].style.width = '500px';
        this.articles[1].style.height = '333px';
        this.articles[1].querySelector('.text').style.opacity = 1;
        this.articles[1].querySelector('.text-aside').style.opacity = 0;
        this.articles[1].querySelector('.gradient').style.opacity = 0;

        this.articles[2].style.width = '300px';
        this.articles[2].style.height = '200px';
        // this.articles[2].querySelector('.text').style.opacity = 1;
        // this.articles[2].querySelector('.text-aside').style.opacity = 1;
        // this.articles[2].querySelector('.gradient').style.opacity = .5;

    }
}
var header = new Header();
header.init();